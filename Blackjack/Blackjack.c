//<Preproccessor>
#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<string.h>
#include<stdbool.h>
#include<iso646.h>
#include<conio.h>
#include<ctype.h>
/*
Assumptions made:
1: dealer plays hard17, no hole.
2: Players aren't permitted to split their hands.
*/

char NUL = '\0';
char STOR[1500];
struct CARD {
	int val;
	char name[55];
	int hole;
	bool color;
};
typedef struct CARD card;
struct DECK {
	card cards[52];
	int lastCard;
	bool shuffled;
};
typedef struct DECK deck;
struct HAND { //TODO restructure functions to accept hands as opposed to players, and remove the dealer boolean value.
	card cards[12];
	int numCards;
	int val; 
	bool blackjack;
	int softVal;
	int betTotal;
};
typedef struct HAND hand;

struct PLAYER {
	hand hnd;
	long int money;
	int busted;
};
typedef struct PLAYER player;
struct GAME {
	player usr;
	player house;
	deck dck;
	int pot;
};
typedef struct GAME game;
void clear(char *arr) {
	memset(arr, 0, strlen(arr));
}
void swap(deck *array, unsigned int a, unsigned int b) {
	card x;
	x = array->cards[a];
	array->cards[a] = array->cards[b];
	array->cards[b] = x;
}
void init(game *g) { //sets the info for all of the cards, and sets correct vals to all player structs objs.
	player *d = &g->house;
	player *p = &g->usr;
	char suits[4][10] = { "Hearts","Diamonds","Clubs","Spades" };
	char names[13][6] = { "Ace","2","3","4","5","6","7","8","9","10","Jack","Queen","King" };
	int vals[13] = { 1,2,3,4,5,6,7,8,9,10,10,10,10 };
	int check = 0;
	g->dck.shuffled = 0;
	for (int x = 0; x < 4; x++) {
		for (int y = 0; y < 13; y++) {
			memset(g->dck.cards[check].name, 0, strlen(g->dck.cards[check].name));
			strcat_s(&g->dck.cards[check].name, 6, names[y]);
			strcat_s(&g->dck.cards[check].name, 4, " of ");
			strcat_s(&g->dck.cards[check].name, 10, suits[x]);
			g->dck.cards[check].val = vals[y];
			g->dck.cards[check].hole = 0;
			check++;
		}
	}
	/*for (int i = 0; i < 52; i++) {
		interDeck->cards[i].hole = 0;
	}*/
	g->dck.lastCard = 51;//starts the array at the top of the stack, and needs to be --'d
	d->hnd.val = 0; d->hnd.betTotal = 0;
	for (int i = 0; i < 12; i++)
	{
		g->house.hnd.cards[i].val = 0;
		g->usr.hnd.cards[i].val = 0;
	}
	p->hnd.val = 0;p->hnd.betTotal = 0;
	printf("H17, single deck, Pushes return your bet, wins pay 1:1, and blackjack pays 3:2.\n");
	printf("No Splits, Dealer plays No Hold Card\n");
}
void shuffle(deck *arr) {
	for (int iter = 51; iter > 1; iter--) {
		srand(time(NULL));
		int j = rand() % iter;
		swap(arr, j, iter);
	}
	arr->shuffled = 1;
	arr->lastCard = 51;
}
void dealOne(player *u, deck *dek) {
	int cardVal = dek->cards[dek->lastCard].val;
	u->hnd.cards[u->hnd.numCards] = dek->cards[dek->lastCard];
	if (cardVal == 1 && u->hnd.softVal == 0)u->hnd.softVal += 10;
	u->hnd.numCards++;
	dek->lastCard--;
	if (u->hnd.val > 21)u->busted = true;
}
bool ai(game *g) //handles dealer actions.
{
	while (g->house.hnd.val < 19) {
		hand *hnd = &g->house.hnd;
		if (hnd->val <= 17) {
			dealOne(&g->house,&g->dck);
		}
		else {
			printf("Stay.\n");
			return true;
		}
	}
}
void bet(game *gm) {
	int bt;
	printf("input the amount of money that you would like to bet.");
	scanf_s("%d", &bt);
	switch (bt <= gm->usr.money) {
	case 0: 
		printf("You don't have that much money.\n");
		bet(gm);
		break;
	case 1:
		gm->usr.hnd.betTotal += bt;
		printf("your bet on this hand is now %d", gm->usr.hnd.betTotal);
		break;
	}
}
game startGame() {
	game gm;
	gm.house.money = 1000000;
	gm.usr.money = 10000;
	gm.pot = 0;
	printf("Please place your opening bet.\n");
	printf("Bet: ");
	shuffle(&gm.dck);
	bet(&gm);
	init(&gm);
	while (gm.usr.hnd.numCards <1) {
		if (gm.usr.hnd.numCards < 1) {
			dealOne(&gm.usr,&gm.dck);
		}
	} //deal hands
	return gm;
}
void printHand(hand h) { //takes a hand, prints it to screen.
	if (h.val != h.softVal) {
		printf("Your hand's value on hard ace is %d, and is %d on soft ace.", h.val, h.softVal);
		}
	else {
		printf("Your hand's value is %d", h.val);
	}
	printf("In your hand, you have:");
	for (int i = 0;h.cards[i].val; i++) {
		printf("%s, ", h.cards[i].name);
	}
}
bool check(hand *h) {
	if (h->softVal == 21) {
		if ((h->cards[0].name == "Ace" and (h->cards[1].name == "Jack" and h->cards[1].color == true))
			or
			(h->cards[1].name == "Ace" and (h->cards[0].name == "Jack" and h->cards[0].color == true))) {
			return true;
		}
	}
}
game playHand(game *g) {
	bool firstChoice = true;
	check(&g->usr.hnd);
	while(g->usr.busted==false) {
		printHand(g->usr.hnd);
		printf("\n you may:\n [H]it, \n or [S]tay");
		if (firstChoice) {
			printf("\nyou may also: [D]ouble Down");
		}
		char input = getch();
		input = toupper(input);
		switch (input) {
		case 'H':
			printf("Hitting...");
			dealOne(&g->usr, &g->dck);
			break;
		case 'S':
			printf("staying...");
			ai(&g);
			break;
		case 'D':
			printf("Are you sure you want to double down? Y/N");
			input = getch();
			switch (toupper(input)) {
			case 'Y':
				bet(&g);
				dealOne(&g->usr, &g->dck);
				break;
			case 'N':
				break;
			default: break;
			}
		default:
			printf("That wasn't one of the given options.");
			break;
		}
		if (g->usr.hnd.val > 21) {
			g->usr.busted = true;
		}
		if (firstChoice) firstChoice = false;
	}
}

int main(void) {

}